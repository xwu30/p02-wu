//
//  MYLabel.m
//  xw2048
//
//  Created by Xian Wu on 2/8/16.
//  Copyright © 2016 Xian Wu. All rights reserved.
//

#import "MYLabel.h"

@implementation MYLabel
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)setNumber:(int)num{
    //NSLog(@"before set num is %d",self.numberTag);
    self.numberTag = num;
}

- (int)getNumberTag{
    return self.numberTag;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (id)init
{
    self = [super init];
    self.numberTag = 512;
    NSLog(@"self.numberTag is %d",self.numberTag);
    if (self) {
        
        self.backgroundColor = [UIColor colorWithRed:246/255.0 green:124/255.0 blue:95/255.0 alpha:0.7];
        self.textAlignment = NSTextAlignmentCenter;
        self.textColor = [UIColor redColor];
        self.layer.cornerRadius=30.0;
        
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
/*- (void)drawRect:(CGRect)rect
{
    // Drawing code
    switch (self.numberTag) {
        case 2:
            //self.backgroundColor = UIColorFromRGB(0xeee4da);
            break;
        case 4:
            //self.backgroundColor = UIColorFromRGB(0xede0c8);
            break;
        case 8:
            self.textColor = UIColorFromRGB(0xf9f6f2);
            self.backgroundColor = UIColorFromRGB(0xf2b179);
            break;
        case 16:
            self.textColor = UIColorFromRGB(0xf9f6f2);
            self.backgroundColor = UIColorFromRGB(0xf59563);
            break;
        case 32:
            self.textColor = UIColorFromRGB(0xf9f6f2);
            self.backgroundColor = UIColorFromRGB(0xf67c5f);
            break;
        case 64:
            self.textColor = UIColorFromRGB(0xf9f6f2);
            self.backgroundColor = UIColorFromRGB(0xf65e3b);
            break;
        case 128:
            self.textColor = UIColorFromRGB(0xf9f6f2);
            self.backgroundColor = UIColorFromRGB(0xedcf72);
            break;
        case 256:
            self.textColor = UIColorFromRGB(0xf9f6f2);
            self.backgroundColor = UIColorFromRGB(0xedcc61);
            break;
        case 512:
            NSLog(@"number tag512 is %d",self.numberTag);
            self.textColor = UIColorFromRGB(0xf9f6f2);
            self.textColor = [UIColor redColor];
            self.backgroundColor = UIColorFromRGB(0xedc850);
            break;
        case 1024:
            NSLog(@"number tag1024 is %d",self.numberTag);
            self.textColor = UIColorFromRGB(0xf9f6f2);
            self.backgroundColor = UIColorFromRGB(0xedc53f);
            break;
        case 2048:
            NSLog(@"number tag2048 is %d",self.numberTag);
            self.textColor = UIColorFromRGB(0xf9f6f2);
            self.backgroundColor = UIColorFromRGB(0xedc22e);
            break;
        default:
            NSLog(@"number tag0 is %d",self.numberTag);
            self.textColor = UIColorFromRGB(0xf9f6f2);
            //self.textColor = [UIColor redColor];
            self.backgroundColor = UIColorFromRGB(0xedc22e);
            break;
    }
    
    [super drawRect:rect];
}
*/

@end
