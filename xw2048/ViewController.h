//
//  ViewController.h
//  xw2048
//
//  Created by Xian Wu on 2/2/16.
//  Copyright © 2016 Xian Wu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "MYLabel.h"
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
@interface ViewController : UIViewController<UIAlertViewDelegate, AVAudioPlayerDelegate>{
    AVAudioPlayer *audioPlayer;
    NSString *audioFile;
}

@property (strong, nonatomic) IBOutlet MYLabel *t00,*t01,*t02,*t03,*t10,*t11,*t12,*t13,*t20,*t21,*t22,*t23,*t30,*t31,*t32,*t33;
@property (strong, nonatomic) IBOutlet UIButton *b0,*b1,*b2,*b3;
@property (strong, nonatomic) IBOutlet UITextField *sco;
@property (strong, nonatomic) IBOutletCollection(MYLabel) NSArray *_tiles;

@property (nonatomic)    AVAudioPlayer  *audioPlayer;
@property (nonatomic)    NSString        *audioFile;
@end

