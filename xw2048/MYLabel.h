//
//  MYLabel.h
//  xw2048
//
//  Created by Xian Wu on 2/8/16.
//  Copyright © 2016 Xian Wu. All rights reserved.
//

#import <UIKit/UIKit.h>

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface MYLabel : UILabel
@property(nonatomic)int numberTag;
@property(nonatomic)int placeTag;
- (void)setNumber:(int)num;
- (int)getNumberTag;
@end
