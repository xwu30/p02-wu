//
//  ViewController.m
//  xw2048
//
//  Created by Xian Wu on 2/2/16.
//  Copyright © 2016 Xian Wu. All rights reserved.
//

#import "ViewController.h"
#import <Foundation/Foundation.h>
#import "MYLabel.h"
@interface ViewController ()



@end

@interface Person: NSObject{
    int age;
    NSString *name;
}
-(void)getname;
-(void)getage;
@end

@implementation Person

int counter = 0;
-(void)getage{
    counter++;
    NSLog(@"getage.....%d",counter);
}
-(void)getname{
    NSLog(@"getname.....%d",counter);
    counter++;
}

@end

@implementation ViewController
@synthesize t00,t01,t02,t03,t10,t11,t12,t13,t20,t21,t22,t23,t30,t31,t32,t33;
@synthesize b0,b1,b2,b3;
@synthesize _tiles;
@synthesize sco;
int score = 0;
int changed[16] = {0};
UIColor *colorr = nil;//original color
NSArray *tils;

- (void)viewDidLoad {
    self._tiles = [NSArray arrayWithObjects: t00,t01,t02,t03,t10,t11,t12,t13,t20,t21,t22,t23,t30,t31,t32,t33, nil];
    colorr = t00.backgroundColor;
    
    [self._tiles setValue:@" " forKey:@"text"];
    [self._tiles setValue:@"0" forKey:@"numberTag"];
    NSLog(@"     %d   ",changed[0]);
    MYLabel *ui = [self._tiles objectAtIndex:1];
    int random;
    for (int i = 0; i <2;) {
        random = arc4random()%(_tiles.count-1);
//        NSString *st = [NSString stringWithFormat:@"%d",random];
//        random = 0;
        ui = [self._tiles objectAtIndex:random];
        if (ui.numberTag == 0) {
            [ui setNumber:2];
            [ui setText:@"2"];
            [self drawColor:ui];
           // [ui drawRect:<#(CGRect)#>]
            i++;
        }
        
        
    }
    
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor orangeColor];
    UIView *v = [[UIView alloc]initWithFrame:CGRectMake(20,20,40,40)];
    v.backgroundColor = [UIColor redColor];
    [self.view addSubview:v];
    NSLog(@"here I am");
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressUp:(id)sender {
    MYLabel *ui = [self._tiles objectAtIndex:1];
    int random;
    int rowp,colp,t1;
    int li;
    int me=0, target=0;
    BOOL move;
    NSString *out;
    MYLabel *temp1, *temp2,*temp3;
    
    switch ((int) [sender tag]) {
        case 0:
            move =[self isMovable:0];
            if(move){
                NSLog(@"its movable up!");
                for(rowp = 1; rowp < 4; rowp++)
                    for(colp = 0; colp <4; colp++){
                        me = 4*rowp+colp;
                        temp1 = [_tiles objectAtIndex:me];
                        for(t1 = rowp; t1>0; t1--){
                            NSLog(@"me%dt1 is %d",me,t1);
                            target = 4*(t1-1)+colp;
                            temp3 = [_tiles objectAtIndex:target];
                            if(changed[target]!=0){
                                NSLog(@"me%dmenumber is %d, and %dtargetnumber is %d",me,temp1.numberTag,target,temp3.numberTag);
                                target = target+4;
                                break;
                            }
                            if(temp3.numberTag!=0&&(temp3.numberTag!=temp1.numberTag))
                            {
                                NSLog(@"me%dnumber is %d, and %dtargetnumber is %d",me,temp1.numberTag,target,temp3.numberTag);
                                target = target+4;
                                break;
                            }

                        }
                        NSLog(@"me is %d, and target is %d",me,target);
                        temp1 = [_tiles objectAtIndex:me];
                        if(temp1.numberTag!=0){
                            if(me != target){
                                
                                
                                temp2 = [_tiles objectAtIndex:target];
                            if(temp2.numberTag == 0 ){
                                [temp2 setNumber:temp1.numberTag];
                                out = [NSString stringWithFormat:@"%d",temp2.numberTag ];
                                li = [out compare: @"0"];
                                if(li != 0)
                                [temp2 setText:out];
                                [temp1 setText:@" "];
                                [temp1 setNumber:0];
                                [self drawColor:temp2];
                                [self drawColor:temp1];
                              
                            }else if(temp1.numberTag==temp2.numberTag){
                                [temp2 setNumber:temp1.numberTag+temp2.numberTag];
                                [temp1 setNumber:0];
                                out = [NSString stringWithFormat:@"%d",temp2.numberTag ];
                                li = [out compare: @"0"];
                                if(li != 0)
                                [temp2 setText:out];
                                [temp1 setText:@" "];
                                [self drawColor:temp2];
                                [self drawColor:temp1];
                                if(li != 0)
                                changed[target] = 1;
                                
                            }
                        }
                    }
                    }
                for (int i = 0; i <1;) {
                    random = arc4random()%(_tiles.count-1);
                    //        NSString *st = [NSString stringWithFormat:@"%d",random];
                    
                    ui = [self._tiles objectAtIndex:random];
                    if (ui.numberTag == 0) {
                        [ui setNumber:2];
                        [ui setText:@"2"];
                        [self drawColor:ui];
                        // [ui drawRect:<#(CGRect)#>]
                        i++;
                    }
                    
                }
            }
            
            break;
            
        case 1:
            move =[self isMovable:1];
            if(move){
                NSLog(@"its movable down!");
                for(rowp = 2; rowp >=0; rowp--)
                    for(colp = 0; colp <4; colp++){
                        me = 4*rowp+colp;
                        temp1 = [_tiles objectAtIndex:me];
                        for(t1 = rowp; t1<3; t1++){
                            
                            target = 4*(t1+1)+colp;
                            temp3 = [_tiles objectAtIndex:target];
                            if(changed[target]!=0){
                                NSLog(@"menumber is %d, and targetnumber is %d",temp1.numberTag,temp3.numberTag);
                                target = target-4;
                                break;
                            }
                            if(temp3.numberTag!=0&&(temp3.numberTag!=temp1.numberTag))
                            {
                                NSLog(@"menumber is %d, and targetnumber is %d",temp1.numberTag,temp3.numberTag);
                                target = target-4;
                                break;
                            }
                        }
                        NSLog(@"me is %d, and target is %d",me,target);
                        temp1 = [_tiles objectAtIndex:me];
                        if(temp1.numberTag!=0){
                            if(me != target){
                                
                                
                                temp2 = [_tiles objectAtIndex:target];
                            if(temp2.numberTag == 0 ){
                                [temp2 setNumber:temp1.numberTag];
                                out = [NSString stringWithFormat:@"%d",temp2.numberTag ];
                                li = [out compare: @"0"];
                                if(li != 0)
                                    [temp2 setText:out];
                                [temp1 setText:@" "];
                                [temp1 setNumber:0];
                                [self drawColor:temp2];
                                [self drawColor:temp1];
                                
                            }else if(temp1.numberTag==temp2.numberTag){
                                [temp2 setNumber:temp1.numberTag+temp2.numberTag];
                                [temp1 setNumber:0];
                                out = [NSString stringWithFormat:@"%d",temp2.numberTag ];
                                li = [out compare: @"0"];
                                if(li != 0)
                                    [temp2 setText:out];
                                [temp1 setText:@" "];
                                [self drawColor:temp2];
                                [self drawColor:temp1];
                                if(li != 0)
                                    changed[target] = 1;
//                                for(int j =0; j < 16;j++){
//                                    NSLog(@"xiachange[%d]=%d",j,changed[j]);}
                            }
                        }
                        }
                    }
                for (int i = 0; i <1;) {
                    random = arc4random()%(_tiles.count-1);
                    //        NSString *st = [NSString stringWithFormat:@"%d",random];
                    
                    ui = [self._tiles objectAtIndex:random];
                    if (ui.numberTag == 0) {
                        [ui setNumber:2];
                        [ui setText:@"2"];
                        [self drawColor:ui];
                        // [ui drawRect:<#(CGRect)#>]
                        i++;
                    }
                    
                }
            }
            
            break;

        case 2:
            move =[self isMovable:2];
            if(move){
                NSLog(@"its movable left!");
                for(colp = 1; colp < 4; colp++)
                    for(rowp = 0; rowp <4; rowp++){
                        me = 4*rowp+colp;
                        temp1 = [_tiles objectAtIndex:me];
                        for(t1 = colp; t1>0; t1--){
                            
                            target = 4*rowp+t1-1;
                            temp3 = [_tiles objectAtIndex:target];
                            if(changed[target]!=0)
                            {
                                NSLog(@"menumber is %d, and targetnumber is %d",temp1.numberTag,temp3.numberTag);
                                target = target+1;
                                break;
                            }
                            if(temp3.numberTag!=0&&(temp3.numberTag!=temp1.numberTag))
                            {
                                NSLog(@"menumber is %d, and targetnumber is %d",temp1.numberTag,temp3.numberTag);
                                target = target+1;
                                break;
                            }
                        }
                        NSLog(@"me is %d, and target is %d",me,target);
                        temp1 = [_tiles objectAtIndex:me];
                        if(temp1.numberTag!=0){
                            if(me != target){
                                
                                
                                temp2 = [_tiles objectAtIndex:target];
                            if(temp2.numberTag == 0 ){
                                [temp2 setNumber:temp1.numberTag];
                                out = [NSString stringWithFormat:@"%d",temp2.numberTag ];
                                li = [out compare: @"0"];
                                if(li != 0)
                                    [temp2 setText:out];
                                [temp1 setText:@" "];
                                [temp1 setNumber:0];
                                [self drawColor:temp2];
                                [self drawColor:temp1];
                            }else if(temp1.numberTag==temp2.numberTag){
                                [temp2 setNumber:temp1.numberTag+temp2.numberTag];
                                [temp1 setNumber:0];
                                out = [NSString stringWithFormat:@"%d",temp2.numberTag ];
                                li = [out compare: @"0"];
                                if(li != 0)
                                    [temp2 setText:out];
                                [temp1 setText:@" "];
                                [self drawColor:temp2];
                                [self drawColor:temp1];
                                if(li != 0)
                                    changed[target] = 1;
                                
                            }
                        }
                        }
                    }
                for (int i = 0; i <1;) {
                    random = arc4random()%(_tiles.count-1);
                    //        NSString *st = [NSString stringWithFormat:@"%d",random];
                    
                    ui = [self._tiles objectAtIndex:random];
                    if (ui.numberTag == 0) {
                        [ui setNumber:2];
                        [ui setText:@"2"];
                        [self drawColor:ui];
                        // [ui drawRect:<#(CGRect)#>]
                        i++;
                    }
                    
                }
            }
            
            break;

        case 3:
            move =[self isMovable:3];
            NSLog(@"%d",move);
            if(move){
                NSLog(@"its movable right!");
                for(colp = 2; colp >=0; colp--)
                    for(rowp = 0; rowp <4; rowp++){
                        me = 4*rowp+colp;
                        temp1 = [_tiles objectAtIndex:me];
                        for(t1 = colp; t1<3; t1++){
                            
                            target = 4*rowp+t1+1;
                            temp3 = [_tiles objectAtIndex:target];
                            if(changed[target]!=0){
                                NSLog(@"menumber is %d, and targetnumber is %d",temp1.numberTag,temp3.numberTag);
                                target = target-1;
                                break;
                            }
                            if(temp3.numberTag!=0&&(temp3.numberTag!=temp1.numberTag))
                            {
                                NSLog(@"menumber is %d, and targetnumber is %d",temp1.numberTag,temp3.numberTag);
                                target = target-1;
                                break;
                            }
                        }
                        NSLog(@"me is %d, and target is %d",me,target);
                        temp1 = [_tiles objectAtIndex:me];
                        if(temp1.numberTag!=0){
                        if(me != target){
                            
                            
                            temp2 = [_tiles objectAtIndex:target];
                            if(temp2.numberTag == 0 ){
                                [temp2 setNumber:temp1.numberTag];
                                out = [NSString stringWithFormat:@"%d",temp2.numberTag ];
                                li = [out compare: @"0"];
                                if(li != 0)
                                    [temp2 setText:out];
                                [temp1 setText:@" "];
                                [temp1 setNumber:0];
                                [self drawColor:temp2];
                                [self drawColor:temp1];
                             
                            }else if(temp1.numberTag==temp2.numberTag){
                                [temp2 setNumber:temp1.numberTag+temp2.numberTag];
                                [temp1 setNumber:0];
                                out = [NSString stringWithFormat:@"%d",temp2.numberTag ];
                                li = [out compare: @"0"];
                                if(li != 0)
                                    [temp2 setText:out];
                                [temp1 setText:@" "];
                                [self drawColor:temp2];
                                [self drawColor:temp1];
                                if(li != 0)
                                    changed[target] = 1;
                            }
                        }
                        }
                    }
                for (int i = 0; i <1;) {
                    random = arc4random()%(_tiles.count-1);
                    //        NSString *st = [NSString stringWithFormat:@"%d",random];
                    
                    ui = [self._tiles objectAtIndex:random];
                    if (ui.numberTag == 0) {
                        [ui setNumber:2];
                        [ui setText:@"2"];
                        [self drawColor:ui];
                        // [ui drawRect:<#(CGRect)#>]
                        i++;
                    }
                    
                }
            }
            
            break;

            
        default:
            break;
    }
    
    for(rowp = 0; rowp <16;rowp++)
        changed[rowp]=0;//reset the status of tiles
    NSLog(@"pressUp Button %d", (int)[sender tag]);
//    [t02 setNumber:32];
//        NSLog(@"after set num is %d",t02.numberTag);
//    [t02 setText: @"512"];
//    [self drawColor:t02];
//    [t00 setNumber:2048];
//    [t00 setText: @"2048"];
//    [self drawColor:t00];
}

-(BOOL)isMovable:(int) dir{
    MYLabel *temp1,*label;
//    NSLog(@"in the function");
    for (int i = 0; i < 16; i++) {

        label = [self._tiles objectAtIndex:i];
        if(label.numberTag == 0)continue;
        NSLog(@"in the for");
        switch (dir) {
            case 3:
                if ((label.tag%4)!=3){
                    temp1=[_tiles objectAtIndex:(label.tag + 1)];
                    if((temp1.numberTag == label.numberTag)||(temp1.numberTag == 0))
                        return YES;
                }
                break;
            case 2:
                if ((label.tag%4)!=0){
                    temp1=[_tiles objectAtIndex:(label.tag - 1)];
                    if((temp1.numberTag == label.numberTag)||(temp1.numberTag == 0))
                        return YES;
                }
                break;
            case 1:
                if (label.tag<12){
                    temp1=[_tiles objectAtIndex:(label.tag + 4)];
                    if((temp1.numberTag == label.numberTag)||(temp1.numberTag == 0))
                        return YES;
                }
                break;
            case 0:
                                NSLog(@"its movable? to check");
                if (label.tag>3){
                    temp1=[_tiles objectAtIndex:(label.tag - 4)];
                    if((temp1.numberTag == label.numberTag)||(temp1.numberTag == 0))
                        return YES;
                }
                break;
            default:
                break;
        }
    }
    NSLog(@"   not moveable!");
    return NO;
}

- (void)drawColor:(MYLabel*)my
{
    // Drawing code
    switch (my.numberTag) {
        case 2:
            my.backgroundColor = UIColorFromRGB(0xeee4da);
            break;
        case 4:
            my.backgroundColor = [UIColor brownColor];
            break;
        case 8:
            //my.textColor = UIColorFromRGB(0xf9f6f2);
            my.backgroundColor = [UIColor yellowColor];
            break;
        case 16:
            //my.textColor = UIColorFromRGB(0xf9f6f2);
            my.backgroundColor = [UIColor grayColor];
            break;
        case 32:
            //my.textColor = UIColorFromRGB(0xf9f6f2);
            my.backgroundColor = [UIColor darkGrayColor];
            //my.backgroundColor = [UIColor greenColor];
            break;
        case 64:
            //my.textColor = UIColorFromRGB(0xf9f6f2);
            my.backgroundColor = [UIColor clearColor];
            break;
        case 128:
            //my.textColor = UIColorFromRGB(0xf9f6f2);
            my.backgroundColor = [UIColor lightGrayColor];
            break;
        case 256:
            //my.textColor = UIColorFromRGB(0xf9f6f2);
            my.backgroundColor = [UIColor magentaColor];
            break;
        case 512:
            //NSLog(@"number tag512 is %d",my.numberTag);
            //my.textColor = UIColorFromRGB(0xf9f6f2);
            my.backgroundColor = [UIColor redColor];
            break;
        case 1024:
            //NSLog(@"number tag1024 is %d",my.numberTag);
            //my.textColor = UIColorFromRGB(0xf9f6f2);
            my.backgroundColor = [UIColor orangeColor];
            break;
        case 2048:
            //NSLog(@"number tag2048 is %d",my.numberTag);
            //my.textColor = UIColorFromRGB(0xf9f6f2);
            my.backgroundColor = [UIColor purpleColor];
            break;
        default:
            //NSLog(@"number tag0 is %d",my.numberTag);
            //my.textColor = UIColorFromRGB(0xf9f6f2);
            //self.textColor = [UIColor redColor];
            my.backgroundColor = colorr;
            break;
    }
    
}

-(void)compress:(int)dd{
    
}

@end
